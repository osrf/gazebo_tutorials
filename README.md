# REPOSITORY MOVED

## This repository has moved to

https://github.com/osrf/gazebo_tutorials

## Issues and pull requests are backed up at

https://osrf-migration.github.io/gazebo_tutorials-gh-pages/#!/osrf/gazebo_tutorials

## Until BitBucket removes Mercurial support, this read-only mercurial repository will be at

https://bitbucket.org/osrf-migrated/gazebo_tutorials

## More info at

https://community.gazebosim.org/t/important-gazebo-and-ignition-are-going-to-github/533

